use regex::Regex;
use scraper::{Html, Selector};
use serde_json::Value;
use serenity::model::channel::Message;
use serenity::prelude::Context;

pub struct NineGag {
    msg_content: String,
    client: reqwest::Client,
}

impl NineGag {
    pub fn new_from_msg(msg_content: &str) -> Self {
        Self {
            msg_content: msg_content.to_string(),
            client: reqwest::Client::builder()
                .user_agent("Mozilla/5.0")
                .build()
                .unwrap(),
        }
    }

    async fn get_csrf_token(&self) -> String {
        let html = self
            .client
            .get("https://awesofun.com")
            .send()
            .await
            .expect("send")
            .text()
            .await
            .unwrap();

        let document = Html::parse_document(&html);
        let selector = Selector::parse("#imgcsrftoken").unwrap();
        let csrf_token = document.select(&selector).next().unwrap();

        csrf_token.value().attr("class").unwrap().to_owned()
    }

    async fn send_post(&self, ninegag_url: &str, csrf_token: &str) -> String {
        let params = [("url", ninegag_url), ("csrf_token", csrf_token)];
        self.client
            .post("https://awesofun.com")
            .form(&params)
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap()
    }

    async fn get_video_url(&self, html: &str) -> String {
        let document = Html::parse_document(html);
        let selector = Selector::parse("a.video").unwrap();
        let video_url = document.select(&selector).next().unwrap();

        video_url.value().attr("data").unwrap().to_owned()
    }

    async fn is_sensitive_content(&self, ninegag_url: &str) -> bool {
        let html = self
            .client
            .get(ninegag_url)
            .send()
            .await
            .expect("send")
            .text()
            .await
            .unwrap();

        let ugly_json_text = html
            .split("JSON.parse(\"")
            .nth(1)
            .unwrap()
            .split("\");</script>")
            .next()
            .unwrap();
        let json_text = ugly_json_text.replace('\\', "");
        let v: Value = serde_json::from_str(&json_text).unwrap();

        v["data"]["post"]["nsfw"] == 1
    }

    pub async fn contains_ninegag_url(&self) -> Option<String> {
        let re = Regex::new(r".*https://9gag\.com/gag/(?P<id>[0-9A-Za-z]+).*").unwrap();
        let id = re.captures(&self.msg_content)?.name("id")?.as_str();
        Some(id.to_owned())
    }

    pub async fn execute(&self, ctx: &Context, msg: &Message, ninegag_id: &str) {
        let ninegag_url = format!("https://9gag.com/gag/{}", ninegag_id);

        if self.is_sensitive_content(&ninegag_url).await {
            let csrf_token = self.get_csrf_token().await;
            let html = self.send_post(&ninegag_url, &csrf_token).await;
            let video_url = self.get_video_url(&html).await;

            let message = format!("Version non censurée : {}", video_url);

            if let Err(why) = msg.reply(&ctx.http, message).await {
                println!("Error sending message: {:?}", why);
            }
        }
    }
}

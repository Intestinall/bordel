FROM rust:1.61

WORKDIR /app
COPY . .

RUN cargo build --release

CMD ["target/release/bordel"]
